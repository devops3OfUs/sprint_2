<!DOCTYPE html>

<html lang="#rn:language_code#">
	
<head>
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8"/>

<title>Home Page</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>

<body>
	
<header>
	<?/* got bootstrap Navigation Bar from http://getbootstrap.com/getting-started/#download*/?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Log-In <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Sign-Up</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Recipe Uploads</a></li>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Special Of the Week</a></li>
            
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default" >Submit</button>
      </form>
          </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


</header>



<div id="headerImg">
	
	<img src=""/>
	

</div>


<?/*https://www.w3schools.com/howto/howto_css_animated_search.asp*/?>
<div id="search">
	<input type="text" name="search" placeholder="Search..">
</div>

<div id="PageContainer">
</div>



<div id="PageBottom">
</div>
</body>
</html>

<style>


body{
	background:#124470;
	width:100%;
	height:auto;
	font-family:Heiti SC;
}
#PageContainer {
    background: #124470;
    width: 100%;
    height: 200px;
    font-family: Heiti SC;
    margin-top: 100px;
}
.floatLeft{
	color:white;
	width:40%;
	border: 1px solid white;
	margin-top: 100px;
	float:left;
	margin-left: 40px;
	padding: 60px;
}
.floatRight{
	float:right;
	width:40%;
	color:white;
	margin-top: 100px;
}
#RHeader {
    font-weight: 900;
    font-size: 30px;
    text-transform: uppercase;
    text-decoration: underline;
    text-align: center;
}
#special{
	font-weight: bold;
    font-size: 18px;
    text-align: center;

}
img {
    width:100%;
    margin-top:-20px;
}

input[type=text] {
    width: 130px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

/* When the input field gets focus, change its width to 100% */
input[type=text]:focus {
    width: 50%;
}
#search{
	margin-top:-90px;
	margin-left: 30px;
}

#PageBottom {
	background:white;
	height: 300px;
    margin-top: 350px;
    padding-left: 90px;
    padding-top: 30px;
    padding-bottom: 30px;
}
#recipeHeader{
	font-weight: bold;
}

#Buy {
	background:grey;
	width:30px;
	border:none;
    color:white;
    width: 30%;
    padding: 10px;
    margin-top: 10px;
    margin-left:30px;
    margin-bottom: 100px;
    border-radius: 5px;
}

</style>
