<!DOCTYPE html>

<html lang="#rn:language_code#">
	
<head>
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8"/>

<title>Home Page</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>

<body>
	
<header>
	<?/* got bootstrap Navigation Bar from http://getbootstrap.com/getting-started/#download*/?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default" >Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


</header>



<div id="headerImg">
	
	<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwNdH4xqtBZX63dtgXxDH4pU0jGOwPKdqDnw2iojEZknKifI3B"/>
	

</div>


<?/*https://www.w3schools.com/howto/howto_css_animated_search.asp*/?>
<div id="search">
	<input type="text" name="search" placeholder="Search..">
</div>

<div id="PageContainer">
	
	<div class="floatLeft">
		<p id="RHeader">Recipe of the Week</p>
		<p id="special">Change Recipe Here</p>
		
	</div>
	
	<div class="floatRight">
		<p id="foodImage">Image of food item here</p>
	</div>
</div>
		

<div id="recipeDesc">
	<p></p><p></p>
	<h3 id="recipeHeader"><em >Feeds  6 to 9 people</em> </h3>
		<p><ol>Ingredients</ol>
			<li>Crust -box Pillsbury� refrigerated pie crusts, softened as directed on box</li>
	<h3>Filling</h3> 
			<li> 1/3 cup butter or margarine</li> 
			<li> 1/3 cup chopped onion</li> 
			<li>1/3 cup all-purpose flour</li> 
			<li>1/2 teaspoon salt</li> 
			<li>1/4 teaspoon pepper</li> 
			<li>1 3/4 cups Progresso� chicken broth (from 32-oz carton)</li> <li>1/2 cup milk</li>            
			<li> 2 1/2 cups shredded cooked chicken</li> <li>2 cups frozen mixed vegetables thawed</li> 
	<h3>Steps </h3>
			<li>1 Heat oven to 425�F</li> 
			<li>Make pie crusts as directed on box for Two-Crust Pie using 9-inch glass pie pan</li>
			<li>2 In 2-quart saucepan, melt butter over medium heat</li> 
			<li>Add onion; cook 2 minutes, stirring frequently, until tender. Stir in flour, salt and pepper until well blended</li> 
			<li>Gradually stir in broth and milk, cooking and stirring until bubbly and thickened</li>
			<li>3 Stir in chicken and mixed vegetables</li> 
			<li>Remove from heat</li> <li>Spoon chicken mixture into crust-lined pan</li> 
			<li>Top with second crust; seal edge and flute</li> <li>Cut slits in several places in top crust</li>
			<li>4 Bake 30 to 40 minutes or until crust is golden brown. During last 15 to 20 minutes of baking, cover crust edge with strips of foil to prevent excessive browning</li> 
			<li>Let stand 5 minutes before serving.</li>
	</p>	
	</ol>
</div>
<div class="Buttons">
        	<button id="Buy" type="button" >CLICK HERE TO ORDER INGREDIENTS</button>
</div>
	







</body>
</html>

<style>


body{
	background:#124470;
	width:100%;
	height:auto;
	font-family:Heiti SC;
}
#PageContainer {
    background: #124470;
    width: 100%;
    height: 400px;
    font-family: Heiti SC;
    margin-top: 100px;
}
.floatLeft{
	color:white;
	width:40%;
	border: 1px solid white;
	margin-top: 100px;
	float:left;
	margin-left: 40px;
	padding: 60px;
}
.floatRight{
	float:right;
	width:40%;
	color:white;
	margin-top: 100px;
}
#RHeader {
    font-weight: 900;
    font-size: 30px;
    text-transform: uppercase;
    text-decoration: underline;
    text-align: center;
}
#special{
	font-weight: bold;
    font-size: 18px;
    text-align: center;

}
img {
    width:100%;
    margin-top:-20px;
}

input[type=text] {
    width: 130px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

/* When the input field gets focus, change its width to 100% */
input[type=text]:focus {
    width: 50%;
}
#search{
	margin-top:-90px;
	margin-left: 30px;
}

#recipeDesc {
	background:white;
    margin-top: 350px;
    padding-left: 90px;
    padding-top: 30px;
    padding-bottom: 30px;
}
#recipeHeader{
	font-weight: bold;
}

#Buy {
	background:grey;
	width:30px;
	border:none;
    color:white;
    width: 30%;
    padding: 10px;
    margin-top: 10px;
    margin-left:30px;
    margin-bottom: 100px;
    border-radius: 5px;
}

</style>
		

















