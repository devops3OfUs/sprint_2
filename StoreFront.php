<!DOCTYPE html>

<html lang="en">
	
<head>
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8"/>

<title>Home Page</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>

<body>
	
<header>
	<?/* got bootstrap Navigation Bar from http://getbootstrap.com/getting-started/#download*/?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="http://www.governancecouncils.com/images/logo/restaurant.png"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/LogIn.php">Log-In <span class="sr-only">(current)</span></a></li>
        <li><a href="/LogInSwap.php">Sign-Up</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/RecipeUpload.php">Recipe Uploads</a></li>
            <li><a href="/Dashboard.php">Dashboard</a></li>
            <li><a href="/StoreFront.php">Store Front</a></li>
            <li><a href="/SpecOfWk.php">Special Of the Week</a></li>
            

          </ul>
        </li>
      </ul>
      
          </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


</header>



<div id="headerImg">
	<h2> Buy the Item Here! </h2>
	<?/*<img src=""/>*/?>
	

</div>


<?/*https://www.w3schools.com/howto/howto_css_animated_search.asp*/?>
<div id="search">
	<input type="text" name="search" placeholder="Search..">
</div>

<div id="PageContainer">

<div id="PageTop">
	<p id="sauces">Sauces <img src="https://www.budgetbytes.com/wp-content/uploads/2017/02/Creamy-Roasted-Red-Pepper-Sauce-H.jpg" alt="sauce" width="500" height="300"> </p> 
	
</div>

<div id="PageMiddle1">
	<p id="coffee">Coffee <img src="http://www.todayifoundout.com/wp-content/uploads/2015/07/coffee2.png" alt="coffee" width="500" height="300"> </p> 
</div>

<div id="PageBottom">
	<ul>
	
	<p id="tea">Tea <img src="https://az616578.vo.msecnd.net/files/responsive/cover/main/desktop/2016/10/31/636134762242182888-397006761_tea-005.jpg" alt="tea" width="500" height="300"> </p>
	
	
</ul>
</div>
</div>

</body>
</html>

<style>


body{
	background:#124470;
	width:100%;
	height:auto;
	font-family:Heiti SC;
}
#PageContainer {
    background: #124470;
    width: 100%;
    height: 200px;
    font-family: Heiti SC;
    margin-top: 100px;
}
.floatLeft{
	color:white;
	width:40%;
	border: 1px solid white;
	margin-top: 100px;
	float:left;
	margin-left: 40px;
	padding: 60px;
}
.floatRight{
	float:right;
	width:40%;
	color:white;
	margin-top: 100px;
}

#tea{
	color:white;
}
#coffee {
    padding-bottom:  40px;
    margin-top: -60px;
}
#RHeader {
    font-weight: 900;
    font-size: 30px;
    text-transform: uppercase;
    text-decoration: underline;
    text-align: center;
}
#special{
	font-weight: bold;
    font-size: 18px;
    text-align: center;

}


input[type=text] {
    width: 130px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

/* When the input field gets focus, change its width to 100% */
input[type=text]:focus {
    width: 50%;
}
#search{
	margin-top:-90px;
	margin-left: 30px;
}
#PageTop{
	height: 300px;
    padding-left: 90px;
    padding-top: 30px;
    padding-bottom: 30px;
    color:white;
}
#PageMiddle1{
	background:white;
	height: 400px;
    margin-top: 50px;
    padding-left: 90px;
    padding-top: 100px;
    padding-bottom: 30px;
}

#PageBottom {
	height: 300px;
    padding-left: 90px;
    padding-top: 30px;
    padding-bottom: 30px;
    color:white;
	
}
#recipeHeader{
	font-weight: bold;
}

#Buy {
	background:grey;
	width:30px;
	border:none;
    color:white;
    width: 30%;
    padding: 10px;
    margin-top: 10px;
    margin-left:30px;
    margin-bottom: 100px;
    border-radius: 5px;
}
.h2, h2 {
    font-size: 30px;
    color:white;
    text-align: center;
    text-decoration: underline;
    margin-top: 190px;
}

</style>