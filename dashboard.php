<!DOCTYPE html>

<html lang="#rn:language_code#">
	
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8"/>

        <title>Dashboard</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    </head>

    <body>
        <div id="recipeBody">
            <div class="recipeHeader">
                <div class="image">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/d/d4/Italian_dressing.jpg"/>
                </div>
                <div class="recipeName">
                    <h2>Olive Garden <br/>
                        Italian Salad Dressing </h2>
                </div>
            </div>
            <div id="recipeDescription" class="description" hidden="true">
                <br/>
                <p> Feeds 1 person/people</p>
                <h3>Ingredients</h3><br/>
                <p> 1 tablespoon garlic salt, 1 tablespoon onion powder, 1 tablespoon white sugar, 2 tablespoons dried oregano, 1 teaspoon ground black pepper, 1/4 teaspoon dried thyme, 1 teaspoon dried basil, 1 tablespoon dried parsley, 1/4 teaspoon celery salt, 2 tablespoons salt</p>
                <h3>Steps</h3><br/>
                <p>1. In a small bowl, mix together the garlic salt, onion powder, sugar, oregano, pepper, thyme, basil, parsley, celery salt and regular salt. Store in a tightly sealed container. <br/>
    2.To prepare dressing, whisk together 1/4 cup white vinegar, 2/3 cup canola oil, 2 tablespoons water and 2 tablespoons of the dry mix.
                </p>
            </div>
        </div>
        <div id="recipeBody2">
            <div class="recipeHeader">
                <div class="image">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/d/d4/Italian_dressing.jpg"/>
                </div>
                <div class="recipeName">
                    <h2>Olive Garden <br/>
                        Italian Salad Dressing </h2>
                </div>
            </div>
            <div id="recipeDescription2" class="description" hidden="true">
                <br/>
                <p> Feeds 1 person/people</p>
                <h3>Ingredients</h3><br/>
                <p> 1 tablespoon garlic salt, 1 tablespoon onion powder, 1 tablespoon white sugar, 2 tablespoons dried oregano, 1 teaspoon ground black pepper, 1/4 teaspoon dried thyme, 1 teaspoon dried basil, 1 tablespoon dried parsley, 1/4 teaspoon celery salt, 2 tablespoons salt</p>
                <h3>Steps</h3><br/>
                <p>1. In a small bowl, mix together the garlic salt, onion powder, sugar, oregano, pepper, thyme, basil, parsley, celery salt and regular salt. Store in a tightly sealed container. <br/>
    2.To prepare dressing, whisk together 1/4 cup white vinegar, 2/3 cup canola oil, 2 tablespoons water and 2 tablespoons of the dry mix.
                </p>
            </div>
        </div>

    </body>
</html>

<style>
	
body{
	background:#124470;
	width:100%;
	height:auto;
	font-family:Heiti SC;
}
H3,H2{
   color: white;
}    
#PageContainer {
    background: #124470;
    width: 100%;
    height: 200px;
    font-family: Heiti SC;
    margin-top: 100px;
    color:white;
}
.floatLeft{
	color:white;
	width:40%;
	border: 1px solid white;
	margin-top: 100px;
	float:left;
	margin-left: 40px;
	padding: 60px;
}
.floatRight{
	float:right;
	width:40%;
	color:white;
	margin-top: 100px;
}
#RHeader {
    font-weight: 900;
    font-size: 30px;
    text-transform: uppercase;
    text-decoration: underline;
    text-align: center;
}
#special{
	font-weight: bold;
    font-size: 18px;
    text-align: center;

}
img {
    width:20%;
}

input[type=text] {
    width: 130px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

/* When the input field gets focus, change its width to 100% */
input[type=text]:focus {
    width: 50%;
}
#search{
	margin-top:-90px;
	margin-left: 30px;
}

#PageBottom {
	background:white;
	height: 300px;
    margin-top: 350px;
    padding-left: 90px;
    padding-top: 30px;
    padding-bottom: 30px;
}
#recipeHeader{
	font-weight: bold;    
}

#Buy {
	background:grey;
	width:30px;
	border:none;
    color:white;
    width: 30%;
    padding: 10px;
    margin-top: 10px;
    margin-left:30px;
    margin-bottom: 100px;
    border-radius: 5px;
}
.recipeName{
	cursor:pointer;
	font-size: 20px;
	text-decoration: underline;
	text-transform: uppercase;
    float:left;
}
.recipeName:hover{
	color:lightblue;
}
.image{
    float: left;
        
}

    

</style>

<script>
$(document).ready(function(){
$("#recipeBody").mouseover(function(e){
        $("#recipeDescription").show();
	});
   
$("#recipeBody").mouseout(function(e){
        $("#recipeDescription").hide();
	});
$("#recipeBody2").mouseover(function(e){
    $("#recipeDescription2").show();
	});
    
$("#recipeBody2").mouseout(function(e){
        $("#recipeDescription2").hide();
	});
    
});
</script>